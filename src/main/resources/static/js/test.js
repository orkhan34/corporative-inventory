$(function(){
	
	
		$("#chooseLang").click(function(){
			
			$("#languages").toggle();
			
			
		});
		var azFlag = "/img/az.svg";
		var ruFlag = "/img/ru.svg";
		var enFlag = "/img/us.svg";

		var imgNavSel = $('#imgNavSel');
		var imgNavAz = $('#imgNavAz');
		var imgNavEng = $('#imgNavEng');
		var imgNavRu = $('#imgNavRu');

		var spanNavSel = $('#lanNavSel');
		var chosenLang = getUrlVars()["lang"];
		if(chosenLang == "az") {
			imgNavSel.attr("src",azFlag);
			spanNavSel.text("AZ");
		} else if (chosenLang == "en") {
			imgNavSel.attr("src",enFlag);
			spanNavSel.text("ENG");
		} else if (chosenLang == "ru") {
			imgNavSel.attr("src",ruFlag);
			spanNavSel.text("RU");
		}
		
		$( ".language" ).on( "click", function( event ) {
			var currentId = $(this).attr('id');
			if(currentId == "navAz") {
				imgNavSel.attr("src",azFlag);
				spanNavSel.text("AZ");
			} else if (currentId == "navEng") {
				imgNavSel.attr("src",enFlag);
				spanNavSel.text("ENG");
			} else if (currentId == "navRu") {
				imgNavSel.attr("src",ruFlag);
				spanNavSel.text("RU");
			}
			$("#languages").hide();
		});
});

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}