package az.corporative.inventory.security;

import java.util.Set;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import az.corporative.inventory.dao.UserDao;
import az.corporative.inventory.model.Role;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		az.corporative.inventory.model.User userModel = userDao.findByEmail(email);
		if(userModel==null)
			throw new UsernameNotFoundException("Username not found!");
		else{
			List<GrantedAuthority> authorities = new ArrayList<>();
			Set<Role> userRoles = userModel.getRoles();
			userRoles.forEach(u->{
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority(u.getRole());
				authorities.add(authority);
			});
			return new User(email, userModel.getPassword(), authorities);
		}
	}

}
