package az.corporative.inventory.security;

public interface SecurityService {

    String findLoggedInUsername();
    void autologin(String username, String password);
}
