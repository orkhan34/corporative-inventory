package az.corporative.inventory.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import az.corporative.inventory.model.Product;;

public interface ProductDao extends CrudRepository<Product, Long>{

//	@Modifying
//	@Query("update Product set description = ?3, name = ?2, category.id = ?4 where id = ?1")
//	int updateCategoryFeature(Long id,String name, String description, Long categoryId);
}
