package az.corporative.inventory.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import az.corporative.inventory.model.Branch;

public interface BranchDao extends CrudRepository<Branch, Long> {

	@Modifying
	@Query("update Branch set name = ?2 where id = ?1")
	int updateBranch(Long id,String branch);
}
