package az.corporative.inventory.dao;

import org.springframework.data.repository.CrudRepository;

import az.corporative.inventory.model.Role;


public interface RoleDao  extends CrudRepository<Role, Long> {

    Role findByRole(String role);
}
