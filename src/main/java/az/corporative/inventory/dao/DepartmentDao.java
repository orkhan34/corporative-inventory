package az.corporative.inventory.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import az.corporative.inventory.model.Department;;

public interface DepartmentDao extends CrudRepository<Department, Long>{

	@Modifying
	@Query("update Department set description = ?3, name = ?2 where id = ?1")
	int updateCategoryFeature(Long id,String name, String description);
}
