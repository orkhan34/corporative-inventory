package az.corporative.inventory.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import az.corporative.inventory.model.Category;;

public interface CategoryDao extends CrudRepository<Category, Long> {

	@Modifying
	@Query("update Category set name = ?2 where id = ?1")
	int updateCategory(Long id,String category);
}
