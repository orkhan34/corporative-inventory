package az.corporative.inventory.dao;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import az.corporative.inventory.model.User;


public interface UserDao extends CrudRepository<User, Long>{

//	@Transactional
//	User updateUserInfo(User userModel);
	@Transactional(readOnly=true)
	User findByEmail(String email);
}
