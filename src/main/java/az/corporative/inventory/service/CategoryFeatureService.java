package az.corporative.inventory.service;

import az.corporative.inventory.model.CategoryFeatures;

public interface CategoryFeatureService {

	CategoryFeatures createNewCategoryFeatures(CategoryFeatures category);
	Iterable<CategoryFeatures> createBatchCategoryFeatures(Iterable<CategoryFeatures> categories);
    int updateCategoryFeatures(CategoryFeatures category);
    CategoryFeatures findByCategoryFeaturesId(Long categoryId);
    Iterable<CategoryFeatures> findAllCategoryFeatures();
    void removeCategoryFeatures(Long categoryId);
}
