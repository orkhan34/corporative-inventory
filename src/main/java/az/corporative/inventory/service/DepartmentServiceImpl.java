package az.corporative.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import az.corporative.inventory.dao.DepartmentDao;
import az.corporative.inventory.model.Department;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentDao departmentDao;
	
	@Override
	public Department createNewDepartment(Department department) {
		return departmentDao.save(department);
	}

	@Override
	@Transactional
	public int updateCategory(Department department) {
		return departmentDao.updateCategoryFeature(department.getId(), department.getName(), department.getDescription());
	}

	@Override
	public Department findByDepartmentId(Long departmentId) {
		return departmentDao.findOne(departmentId);
	}

	@Override
	public Iterable<Department> findAllDepartments() {
		return departmentDao.findAll();
	}

	@Override
	public void removeDepartment(Long departmentId) {
		departmentDao.delete(departmentId);
	}

}
