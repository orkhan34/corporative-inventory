package az.corporative.inventory.service;

import java.util.HashSet;
import java.util.Set;

import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import az.corporative.inventory.dao.RoleDao;
import az.corporative.inventory.dao.UserDao;
import az.corporative.inventory.model.Role;
import az.corporative.inventory.model.User;
import az.corporative.inventory.util.Roles;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	@Autowired
    private RoleDao roleDao;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public User createNewUser(User userModel) {
	    userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
	    final Set<Role> roles = new HashSet<>();
	    final Role roleUser = roleDao.findByRole(Roles.USER);
	    roles.add(roleUser);
	    userModel.setRoles(roles);
		return userDao.save(userModel);
	}

	@Override
	public User updateUserInfo(User user) {
	    User selectedUser = userDao.findOne(user.getId());
		selectedUser.setPassword(user.getPassword());
		selectedUser.setName(user.getName());
		selectedUser.setSurname(user.getSurname());
		selectedUser.setEmail(user.getEmail());
		return selectedUser;
	}

	@Override
	public User findByUserId(Long userId) {
		return userDao.findOne(userId);
	}

	@Override
	public void deleteByUserId(Long userId) {
		userDao.delete(userId);
	}

	@Override
	public Iterable<User> findAllUsers() {
		return userDao.findAll();
	}

}
