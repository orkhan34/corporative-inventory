package az.corporative.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import az.corporative.inventory.dao.CategoryDao;
import az.corporative.inventory.model.Category;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public Category createNewCategory(Category category) {
        return categoryDao.save(category);
    }

    @Override
    public Iterable<Category> findAllCategories() {
        Iterable<Category> categories=categoryDao.findAll();
        return categories;
    }

    @Override
    public void removeCategory(Long categoryId) {
        categoryDao.delete(categoryId);        
    }

	@Override
	@Transactional
	public int updateCategory(Category category) {
		int catId = categoryDao.updateCategory(category.getId(), category.getName());
		return catId;
	}

	@Override
	public Category findByCategoryId(Long categoryId) {
		return categoryDao.findOne(categoryId);
	}

}
