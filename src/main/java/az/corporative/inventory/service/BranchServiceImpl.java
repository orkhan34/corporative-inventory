package az.corporative.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import az.corporative.inventory.dao.BranchDao;
import az.corporative.inventory.model.Branch;


@Service
public class BranchServiceImpl implements BranchService {

	@Autowired
	private BranchDao branchDao;
	
	@Override
	public Branch createNewBranch(Branch branch) {
		return branchDao.save(branch);
	}

	@Override
	@Transactional
	public int updateBranch(Branch branch) {
		int row = branchDao.updateBranch(branch.getId(), branch.getName());
		return row;
	}

	@Override
	public Branch findByBranchId(Long branchId) {
		return branchDao.findOne(branchId);
	}

	@Override
	public Iterable<Branch> findAllBranches() {
		return branchDao.findAll();
	}

	@Override
	public void removeBranch(Long branchId) {
		branchDao.delete(branchId);
	}

}
