package az.corporative.inventory.service;

import az.corporative.inventory.model.User;

public interface UserService {

	User createNewUser(User userModel);
	User updateUserInfo(User userModel);
	User findByUserId(Long userId);
	Iterable<User> findAllUsers();
	void deleteByUserId(Long userId);
}
