package az.corporative.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import az.corporative.inventory.dao.ProductDao;
import az.corporative.inventory.model.Product;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao productDao;
	
	@Override
	public Product createNewProduct(Product product) {
		return productDao.save(product);
	}

	@Override
	public int updateProduct(Product product) {
		return 0;
	}

	@Override
	public Product findByProductId(Long productId) {
		return productDao.findOne(productId);
	}

	@Override
	public Iterable<Product> findAllProducts() {
		return productDao.findAll();
	}

	@Override
	public void removeProduct(Long productId) {
		productDao.delete(productId);
	}

}
