package az.corporative.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import az.corporative.inventory.dao.CategoryFeaturesDao;
import az.corporative.inventory.model.CategoryFeatures;


@Service
public class CategoryFeatureServiceImpl implements CategoryFeatureService {

	@Autowired
	private CategoryFeaturesDao categoryFeaturesDao;

	@Override
	public CategoryFeatures createNewCategoryFeatures(CategoryFeatures categoryFeatures) {
		return categoryFeaturesDao.save(categoryFeatures);
	}

	@Override
	@Transactional
	public int updateCategoryFeatures(CategoryFeatures features) {
		final int catFeatureId = categoryFeaturesDao.updateCategoryFeature(features.getId(), features.getName(),features.getDescription(),features.getCategory().getId());
		return catFeatureId;
	}

	@Override
	public CategoryFeatures findByCategoryFeaturesId(Long categoryId) {
		return categoryFeaturesDao.findOne(categoryId);
	}

	@Override
	public Iterable<CategoryFeatures> findAllCategoryFeatures() {
		return categoryFeaturesDao.findAll();
	}

	@Override
	public void removeCategoryFeatures(Long categoryId) {
		categoryFeaturesDao.delete(categoryId);
	}

	@Override
	public Iterable<CategoryFeatures> createBatchCategoryFeatures(Iterable<CategoryFeatures> categories) {
		return categoryFeaturesDao.save(categories);
	}
	
	

}
