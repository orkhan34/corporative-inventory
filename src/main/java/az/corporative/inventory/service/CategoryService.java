package az.corporative.inventory.service;


import az.corporative.inventory.model.Category;

public interface CategoryService {

    Category createNewCategory(Category category);
    int updateCategory(Category category);
    Category findByCategoryId(Long categoryId);
    Iterable<Category> findAllCategories();
    void removeCategory(Long categoryId);
}
