package az.corporative.inventory.service;

import az.corporative.inventory.model.Branch;

public interface BranchService {

	Branch createNewBranch(Branch branch);
    int updateBranch(Branch branch);
    Branch findByBranchId(Long branchId);
    Iterable<Branch> findAllBranches();
    void removeBranch(Long branchId);
}
