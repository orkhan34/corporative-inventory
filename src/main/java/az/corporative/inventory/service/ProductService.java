package az.corporative.inventory.service;

import az.corporative.inventory.model.Product;

public interface ProductService {

	Product createNewProduct(Product product);
    int updateProduct(Product product);
    Product findByProductId(Long productId);
    Iterable<Product> findAllProducts();
    void removeProduct(Long productId);
}
