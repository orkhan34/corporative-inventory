package az.corporative.inventory.service;

import az.corporative.inventory.model.Department;

public interface DepartmentService {

    Department createNewDepartment(Department department);
    int updateCategory(Department department);
    Department findByDepartmentId(Long departmentId);
    Iterable<Department> findAllDepartments();
    void removeDepartment(Long departmentId);
}
