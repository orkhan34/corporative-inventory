package az.corporative.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorporateInventoryApplication{

	public static void main(String[] args) {
		SpringApplication.run(CorporateInventoryApplication.class, args);
	}
	
	
}
