package az.corporative.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import az.corporative.inventory.model.Branch;
import az.corporative.inventory.service.BranchService;
import az.corporative.inventory.util.Views;

@Controller
public class BranchController {

	@Autowired
	private BranchService branchService;

	@ModelAttribute("branches")
	public Iterable<Branch> branches() {
		final Iterable<Branch> branchList = branchService.findAllBranches();
		return branchList;
	}

	@GetMapping(value = "/branch")
	public String branchList(Model model) {
		return Views.Branch.LIST;
	}

	@GetMapping(value = "/new-branch")
	public String branchAddPage() {
		return Views.Branch.ADD;
	}
	
	@GetMapping(value = "/branch-delete")
	public String branchRemove(@RequestParam(value = "branchId", required = true) Long branchId, Model model) {
		branchService.removeBranch(branchId);
		return "redirect:/branch";
	}

	@PostMapping(value = "/branch-add")
	public String branchCreate(Branch branch) {
		branchService.createNewBranch(branch);
		return "redirect:/branch";
	}

	@PostMapping(value = "/branch-update")
	public String branchUpdate(Branch branch) {
		branchService.updateBranch(branch);
		return "redirect:/branch";
	}

	@GetMapping(value = "/branch-edit")
	public String branchEdit(Model model, @RequestParam(value = "branchId", required = true) Long branchId) {
		final Branch branch = branchService.findByBranchId(branchId);
		model.addAttribute("branchObject", branch);
		return Views.Branch.UPDATE;
	}
}
