package az.corporative.inventory.controller;

import java.io.Serializable;
import java.util.List;

public class CategoryItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Items> items;

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}
	
	
}
