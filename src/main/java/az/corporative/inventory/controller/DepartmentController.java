package az.corporative.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import az.corporative.inventory.model.Department;
import az.corporative.inventory.service.DepartmentService;
import az.corporative.inventory.util.Views;


@Controller
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	
	@ModelAttribute("departments")
	public Iterable<Department> departments() {
		final Iterable<Department> departments = departmentService.findAllDepartments();
		return departments;
	}

	@GetMapping(value = "/departments")
	public String departmentList(Model model) {
		return Views.Department.LIST;
	}

	@GetMapping(value = "/new-department")
	public String departmentAddPage(Model model) {
		model.addAttribute("department", new Department());
		return Views.Department.ADD;
	}
	
	@GetMapping(value = "/department-delete")
	public String departmentRemove(@RequestParam(value = "departmentId", required = true) Long departmentId, Model model) {
		departmentService.removeDepartment(departmentId);
		return "redirect:/departments";
	}

	@PostMapping(value = "/department-add")
	public String departmentCreate(@RequestParam(value="name") String name,
			@RequestParam(value="description") String description) {
		Department department = new Department(name, description);
		departmentService.createNewDepartment(department);
		return "redirect:/departments";
	}

	@PostMapping(value = "/department-update")
	public String departmentUpdate(@RequestParam(value="name") String name
			,@RequestParam(value="description") String description
			,@RequestParam(value="id") Long departmentId) {
		departmentService.updateCategory(new Department(departmentId, name, description));
		return "redirect:/departments";
	}

	@GetMapping(value = "/department-edit")
	public String departmentEdit(Model model, 
			@RequestParam(value = "departmentId", required = true) Long departmentId) {
		final Department department = departmentService.findByDepartmentId(departmentId);
		model.addAttribute("departmentObject", department);
		return Views.Department.UPDATE;
	}
}
