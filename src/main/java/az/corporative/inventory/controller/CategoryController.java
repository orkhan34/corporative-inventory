package az.corporative.inventory.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import az.corporative.inventory.model.Category;
import az.corporative.inventory.model.CategoryFeatures;
import az.corporative.inventory.service.CategoryFeatureService;
import az.corporative.inventory.service.CategoryService;
import az.corporative.inventory.util.Views;

@Controller
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CategoryFeatureService featureService;

	@ModelAttribute("categories")
	public Iterable<Category> categories() {
		final Iterable<Category> categoryList = categoryService.findAllCategories();
		return categoryList;
	}

	@GetMapping(value = "/category")
	public String categoryList(Model model) {
		return Views.Category.LIST;
	}

	@GetMapping(value = "/new-category")
	public String categoryAddPage(Model model) {
		model.addAttribute("categoryFeatures",featureService.findAllCategoryFeatures());
		return Views.Category.ADD;
	}
	
	@GetMapping(value = "/category-delete")
	public String categoryRemove(@RequestParam(value = "categoryId", required = true) Long categoryId, Model model) {
		categoryService.removeCategory(categoryId);
		return "redirect:/category";
	}

	@PostMapping(value = "/category-add",consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String categoryCreate(@RequestBody(required=true) List<Items> items) {
		
		Category category = new Category();
		category.setName(items.get(0).getCategory());
		final Category createdCategory = categoryService.createNewCategory(category);
		final List<CategoryFeatures> list = new ArrayList<>();
		items.forEach(item->{
			final CategoryFeatures categoryFeatures = new CategoryFeatures();
			categoryFeatures.setDescription(item.getDescription());
			categoryFeatures.setName(item.getName());
			categoryFeatures.setCategory(createdCategory);
			list.add(categoryFeatures);
		});
		featureService.createBatchCategoryFeatures(list);
		
		return "redirect:/category";
	}

	@PostMapping(value = "/category-update")
	public String categoryUpdate(Category category) {
		categoryService.updateCategory(category);
		return "redirect:/category";
	}

	@GetMapping(value = "/category-edit")
	public String categoryEdit(Model model, @RequestParam(value = "categoryId", required = true) Long categoryId) {
		final Category category = categoryService.findByCategoryId(categoryId);
		model.addAttribute("categoryObject", category);
		return Views.Category.UPDATE;
	}
}
