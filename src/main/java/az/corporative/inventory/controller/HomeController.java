package az.corporative.inventory.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import az.corporative.inventory.util.Views;


@Controller
public class HomeController {

	
	@GetMapping(value={"","/","/home","/index"})
	public String index(){
		
		return Views.Admin.DASHBOARD;
	}
	
}
