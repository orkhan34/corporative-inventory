package az.corporative.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import az.corporative.inventory.model.Category;
import az.corporative.inventory.model.CategoryFeatures;
import az.corporative.inventory.service.CategoryFeatureService;
import az.corporative.inventory.service.CategoryService;
import az.corporative.inventory.util.Views;


@Controller
public class CategoryFeatureController {

	@Autowired
	private CategoryFeatureService featureService;
	@Autowired
	private CategoryService categoryService;
	
	
	@ModelAttribute("categoryFeatures")
	public Iterable<CategoryFeatures> categoryFeatures() {
		final Iterable<CategoryFeatures> featureList = featureService.findAllCategoryFeatures();
		return featureList;
	}

	@ModelAttribute("categories")
	public Iterable<Category> categories() {
		final Iterable<Category> categoryList = categoryService.findAllCategories();
		return categoryList;
	}
	
//	@GetMapping(value = "/add-category-cache")
//	public String categoryAddCache(Model model) {
//		CacheMap cacheMap = new 
//		return Views.CategoryFeatures.LIST;
//	}
	
	@GetMapping(value = "/feature")
	public String categoryFeatureList(Model model) {
		return Views.CategoryFeatures.LIST;
	}

	@GetMapping(value = "/new-category-feature")
	public String categoryFeatureAddPage(Model model) {
		model.addAttribute("categoryFeatures", new CategoryFeatures());
		return Views.CategoryFeatures.ADD;
	}
	
	@GetMapping(value = "/category-feature-delete")
	public String categoryFeatureRemove(@RequestParam(value = "categoryFeatureId", required = true) Long categoryFeatureId, Model model) {
		featureService.removeCategoryFeatures(categoryFeatureId);
		return "redirect:/feature";
	}

	@PostMapping(value = "/category-feature-add")
	public String categoryFeatureCreate(@RequestParam(value="name") String name,
			@RequestParam(value="description") String description
			,@RequestParam(value="categoryId") Category category) {
		CategoryFeatures categoryFeatures = new CategoryFeatures(category, name, description);
		featureService.createNewCategoryFeatures(categoryFeatures);
		return "redirect:/feature";
	}

	@PostMapping(value = "/category-feature-update")
	public String categoryFeatureUpdate(CategoryFeatures categoryFeatures
			,@RequestParam(value="category") Category category) {
		categoryFeatures.setCategory(category);
		featureService.updateCategoryFeatures(categoryFeatures);
		return "redirect:/feature";
	}

	@GetMapping(value = "/category-feature-edit")
	public String categoryFeatureEdit(Model model, @RequestParam(value = "categoryFeatureId", required = true) Long categoryFeatureId) {
		final CategoryFeatures categoryFeatures = featureService.findByCategoryFeaturesId(categoryFeatureId);
		model.addAttribute("categoryFeatureObject", categoryFeatures);
		return Views.CategoryFeatures.UPDATE;
	}
}
