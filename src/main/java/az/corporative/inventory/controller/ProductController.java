package az.corporative.inventory.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import az.corporative.inventory.model.Branch;
import az.corporative.inventory.model.Category;
import az.corporative.inventory.model.Department;
import az.corporative.inventory.model.Product;
import az.corporative.inventory.model.User;
import az.corporative.inventory.service.BranchService;
import az.corporative.inventory.service.CategoryService;
import az.corporative.inventory.service.DepartmentService;
import az.corporative.inventory.service.ProductService;
import az.corporative.inventory.service.UserService;
import az.corporative.inventory.util.Views;


@Controller
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private BranchService branchService;
	@Autowired
	private UserService userService;
	
	@ModelAttribute("products")
	public Iterable<Product> products() {
		final Iterable<Product> products = productService.findAllProducts();
		return products;
	}

	@GetMapping(value = "/products")
	public String productList(Model model) {
		return Views.Product.LIST;
	}

	@GetMapping(value = "/new-product")
	public String productAddPage(Model model) {
		model.addAttribute("product", new Product());
		model.addAttribute("users", userService.findAllUsers());
		model.addAttribute("departments", departmentService.findAllDepartments());
		model.addAttribute("branches", branchService.findAllBranches());
		model.addAttribute("categories", categoryService.findAllCategories());
		return Views.Product.ADD;
	}
	
	@GetMapping(value = "/product-delete")
	public String productRemove(@RequestParam(value = "productId", required = true) Long productId, Model model) {
		productService.removeProduct(productId);
		return "redirect:/products";
	}

	@PostMapping(value = "/product-add")
	public String productCreate(@RequestParam(value="name") String name,
								@RequestParam(value="description") String description,
								@RequestParam(value="price") BigDecimal price
								,Category category
								,Department department
								,Branch branch
								,User user) {
		Product product = new Product(branch, category, department, name, description, user, price);
		productService.createNewProduct(product);
		return "redirect:/products";
	}

	@PostMapping(value = "/product-update")
	public String productUpdate(@RequestParam(value="name") String name
			,@RequestParam(value="description") String description
			,@RequestParam(value="id") Long departmentId) {
//		departmentService.updateCategory(new Department(departmentId, name, description));
		return "redirect:/products";
	}

	@GetMapping(value = "/product-edit")
	public String productEdit(Model model, 
			@RequestParam(value = "productId", required = true) Long productId) {
		final Product product = productService.findByProductId(productId);
		model.addAttribute("productObject", product);
		return Views.Product.UPDATE;
	}
}
