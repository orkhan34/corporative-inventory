package az.corporative.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import az.corporative.inventory.model.User;
import az.corporative.inventory.security.SecurityService;
import az.corporative.inventory.service.UserService;
import az.corporative.inventory.util.Views;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;
    
    @PostMapping(value="/register")
    public String createNewUser(User user) {
        final User newUser = userService.createNewUser(user);
        securityService.autologin(newUser.getEmail(), newUser.getPassword());
        return "redirect:/"+Views.Admin.DASHBOARD;
    }
    
    @GetMapping(value = "/login")
    public String loginPage(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", error);
       if (logout != null)
            model.addAttribute("logout", "You have been logged out successfully!");
        return Views.User.LOGIN;
    }
}
