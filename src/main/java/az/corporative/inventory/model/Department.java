package az.corporative.inventory.model;
// Generated Dec 6, 2017 11:23:22 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Department generated by hbm2java
 */
@Entity
@Table(name = "department", catalog = "corporativeinventory")
public class Department implements java.io.Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = 179998300833364148L;
    private Long id;
	private String name;
	private String description;
	private Set<Product> products = new HashSet<>(0);

	
	public Department(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Department(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Department() {
	}

	public Department(String name, String description, Set<Product> products) {
		this.name = name;
		this.description = description;
		this.products = products;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
	public Set<Product> getProducts() {
		return this.products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

}
