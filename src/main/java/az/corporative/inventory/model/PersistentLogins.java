package az.corporative.inventory.model;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Component
@Entity
public class PersistentLogins implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 3247370983271369948L;
    //Orkhan M. /remember -me feature
    /**
     * 
     */
    @Id
    @GeneratedValue
    private long id;
    private String username;
    private String series;
    private String token;
    @Column(name="last_used")
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date lastUsed;
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getSeries() {
        return series;
    }
    public void setSeries(String series) {
        this.series = series;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public Date getLastUsed() {
        return lastUsed;
    }
    public void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }
    
}