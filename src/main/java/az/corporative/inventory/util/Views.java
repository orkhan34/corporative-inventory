package az.corporative.inventory.util;

public interface Views {

    String VIEW = "/views";
    String ADMIN = VIEW+"/admin";
    String CATEGORY = ADMIN+"/category";
    String BRANCH = ADMIN+"/branch";
    String CATEGORY_FEATURES = ADMIN+"/category-features";
    String DEPARTMENT = ADMIN+"/department";
    String PRODUCT = ADMIN+"/product";
    
    interface Admin{
        String DASHBOARD = ADMIN + "/dashboard";
    }
    
    interface User{
        String LOGIN = VIEW + "/login";
    }
    
    interface Category{
        String ADD = CATEGORY + "/category-add";
        String UPDATE = CATEGORY + "/category-update";
        String LIST = CATEGORY + "/category-list";
    }
    
    interface Branch{
        String ADD = BRANCH + "/branch-add";
        String UPDATE = BRANCH + "/branch-update";
        String LIST = BRANCH + "/branch-list";
    }
    
    interface CategoryFeatures{
        String ADD = CATEGORY_FEATURES + "/category-features-add";
        String UPDATE = CATEGORY_FEATURES + "/category-features-update";
        String LIST = CATEGORY_FEATURES + "/category-features-list";
    }

    interface Department{
        String ADD = DEPARTMENT + "/department-add";
        String UPDATE = DEPARTMENT + "/department-update";
        String LIST = DEPARTMENT + "/department-list";
    }
    
    interface Product{
        String ADD = PRODUCT + "/product-add";
        String UPDATE = PRODUCT + "/product-update";
        String LIST = PRODUCT + "/product-list";
    }
   
   
}
