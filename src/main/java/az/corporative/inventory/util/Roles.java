package az.corporative.inventory.util;

public interface Roles {

    String ADMIN = "ROLE_ADMIN";
    String USER = "ROLE_USER";
    String SUPER_ADMIN = "ROLE_SUPER_ADMIN";
}
